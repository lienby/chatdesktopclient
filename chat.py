import HTMLParser
import tkMessageBox
import requests
import urllib

import sys


def cleanupString(string):
    string = urllib.unquote(string).decode('utf8')
    return HTMLParser.HTMLParser().unescape(string).encode(sys.getfilesystemencoding())

def get_session_id():
    header = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.94 Chrome/62.0.3202.94 Safari/537.36"}
    return requests.get("http://chatlol.x10host.com",headers=header).cookies.get("PHPSESSID")

global sessid
global uname
def login(email,password,remember=False):
    global sessid
    global uname
    sessid = get_session_id()
    header = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.94 Chrome/62.0.3202.94 Safari/537.36"}
    postdata = {"email":email,"password":password,"loginBtn":"Sign in!"}
    cookies = {"PHPSESSID": sessid}
    while True:
        try:
            r = requests.post("http://chatlol.x10host.com/signin.php",data=postdata,cookies=cookies,headers=header)
            break
        except:
            print "Connectin Failed. Retrying.."
    if r.text.__contains__("<font color='#4f4f4f'>"):
        a = r.text.index("<font color='#4f4f4f'>")+22
        uname = r.text[a:]
        a = uname.index("</font>")
        uname = uname[:a]
        if remember == True:
            open("uname.txt","wb").write(uname)
            open("sessid.txt","wb").write(sessid)
        return True
    else:
        tkMessageBox.showerror(message="Your login information was incorrect!")
        return False

def signup(name,email,chatname,password):
    sessid = get_session_id()
    header = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.94 Chrome/62.0.3202.94 Safari/537.36"}
    postdata = {"name": name, "email": email, "chatname": chatname,"password":password,"passwordconfirm":password,"goBtn":"Let's go!"}
    cookies = {"PHPSESSID": sessid}
    while True:
        try:
            r = requests.post("http://chatlol.x10host.com/createacc.php", data=postdata, cookies=cookies, headers=header)
            break
        except:
            print "Connection Failed. Retrying..."
    if r.text.__contains__("Success"):
        login(email,password,remember=True)
        return True
    elif r.text.__contains__("Error"):
        a = r.text.index("Error: ")+7
        errorcode = r.text[a:]
        a = errorcode.index('\n')
        errorcode = errorcode[:a]
        tkMessageBox.showerror(message=errorcode)
        return False
    else:
        tkMessageBox.showerror(message="An uknown error occured.")
        return False
def send_message(sessid, uname, message):
    header = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.94 Chrome/62.0.3202.94 Safari/537.36","Cookie": "PHPSESSID=" + sessid}
    message = urllib.quote(message)
    uname = urllib.quote(uname)
    while True:
        try:
            r = requests.get("http://chatlol.x10host.com/Chat/insert.php?uname={}&msg={}".format(uname, message), headers=header)
            if r.status_code != 200:
                print "Connection Failed. retrying.."
            else:
                break
        except:
            print "Connection failed. retrying.."

def get_message_log():
    header = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.94 Chrome/62.0.3202.94 Safari/537.36"}
    while True:
        try:
            return requests.get("http://chatlol.x10host.com/Chat/logs.php", headers=header).text
        except:
            print "Connection failed. retrying.."


