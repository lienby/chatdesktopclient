import StringIO
import threading

import os

import chat
from chat import get_message_log, send_message
from Tkinter import *


try:
    import ttk
    py3 = 0
except ImportError:
    import tkinter.ttk as ttk
    py3 = 1



def init(top, gui, *args, **kwargs):
    global w, top_level, root
    w = gui
    top_level = top
    root = top




def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = Tk()
    top = Chat (root)
    init(root, top)
    root.protocol('WM_DELETE_WINDOW', lambda: os._exit(1))
    root.mainloop()

w = None
def create_Chat(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = Toplevel (root)
    top = Chat (w)
    init(w, top, *args, **kwargs)
    return (w, top)

def destroy_Chat():
    global w
    w.destroy()
    w = None


class Chat:
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85' 
        _ana2color = '#d9d9d9' # X11 color: 'gray85' 
        self.style = ttk.Style()
        if sys.platform == "win32":
            self.style.theme_use('winnative')
        self.style.configure('.',background=_bgcolor)
        self.style.configure('.',foreground=_fgcolor)
        self.style.map('.',background=
            [('selected', _compcolor), ('active',_ana2color)])

        top.geometry("600x450+329+104")
        top.title("Chat")
        top.configure(highlightcolor="black")



        self.Input = Entry(top)
        self.Input.place(relx=0.02, rely=0.89, relheight=0.09, relwidth=0.74)
        self.Input.configure(background="white")
        self.Input.configure(font="TkFixedFont")
        self.Input.configure(selectbackground="#c4c4c4")
        self.Input.bind('<Return>',lambda e: sm())


        self.send = Button(top)
        self.send.place(relx=0.78, rely=0.88, height=45, width=107)
        self.send.configure(activebackground="#d9d9d9")
        self.send.configure(command=lambda: sm())
        self.send.configure(text='''Send Message''')

        self.chatbox = ScrolledText(top)
        self.chatbox.place(relx=0.02, rely=0.02, relheight=0.84, relwidth=0.96)
        self.chatbox.configure(background="white")
        self.chatbox.configure(font="TkTextFont")
        self.chatbox.configure(insertborderwidth="3")
        self.chatbox.configure(selectbackground="#c4c4c4")
        self.chatbox.configure(width=10)
        self.chatbox.configure(wrap=NONE)
        self.chatbox.bind("<Key>", lambda e: "break")

        def sm():
            if self.Input.get() != "":
                b = threading.Thread(target=send_message, args=(chat.sessid, chat.uname, self.Input.get()))
                b.start()
                self.chatbox.insert(END,chat.cleanupString(chat.uname)+": "+self.Input.get()+"\n")
                self.chatbox.see(END)
                self.Input.delete(0, END)
        def post_messages():
            msg = get_message_log()
            msg = msg.replace("<br>", "\n")
            msg = re.sub(r'<.*?>', '', msg)
            msg = chat.cleanupString(msg)
            self.chatbox.insert(1.0, msg)
            self.chatbox.see(END)
            buf = StringIO.StringIO(msg)
            lastentered = buf.readlines()[-1]
            while True:
                msg = get_message_log()
                msg = msg.replace("<br>", "\n")
                msg = re.sub(r'<.*?>', '', msg)
                msg = chat.cleanupString(msg)
                buf = StringIO.StringIO(msg)
                msg = buf.readlines()[-1]
                if msg != lastentered and not msg.startswith(chat.cleanupString(chat.uname)):
                    self.chatbox.insert(END,msg)
                    lastentered = msg
                    self.chatbox.see(END)
        t = threading.Thread(target=post_messages)
        t.start()




# The following code is added to facilitate the Scrolled widgets you specified.
class AutoScroll(object):
    '''Configure the scrollbars for a widget.'''

    def __init__(self, master):
        #  Rozen. Added the try-except clauses so that this class
        #  could be used for scrolled entry widget for which vertical
        #  scrolling is not supported. 5/7/14.
        try:
            vsb = ttk.Scrollbar(master, orient='vertical', command=self.yview)
        except:
            pass
        hsb = ttk.Scrollbar(master, orient='horizontal', command=self.xview)

        #self.configure(yscrollcommand=_autoscroll(vsb),
        #    xscrollcommand=_autoscroll(hsb))
        try:
            self.configure(yscrollcommand=self._autoscroll(vsb))
        except:
            pass
        self.configure(xscrollcommand=self._autoscroll(hsb))

        self.grid(column=0, row=0, sticky='nsew')
        try:
            vsb.grid(column=1, row=0, sticky='ns')
        except:
            pass
        hsb.grid(column=0, row=1, sticky='ew')

        master.grid_columnconfigure(0, weight=1)
        master.grid_rowconfigure(0, weight=1)

        # Copy geometry methods of master  (taken from ScrolledText.py)
        if py3:
            methods = Pack.__dict__.keys() | Grid.__dict__.keys() \
                  | Place.__dict__.keys()
        else:
            methods = Pack.__dict__.keys() + Grid.__dict__.keys() \
                  + Place.__dict__.keys()

        for meth in methods:
            if meth[0] != '_' and meth not in ('config', 'configure'):
                setattr(self, meth, getattr(master, meth))

    @staticmethod
    def _autoscroll(sbar):
        '''Hide and show scrollbar as needed.'''
        def wrapped(first, last):
            first, last = float(first), float(last)
            if first <= 0 and last >= 1:
                sbar.grid_remove()
            else:
                sbar.grid()
            sbar.set(first, last)
        return wrapped

    def __str__(self):
        return str(self.master)

def _create_container(func):
    '''Creates a ttk Frame with a given master, and use this new frame to
    place the scrollbars and the widget.'''
    def wrapped(cls, master, **kw):
        container = ttk.Frame(master)
        return func(cls, container, **kw)
    return wrapped

class ScrolledText(AutoScroll, Text):
    '''A standard Tkinter Text widget with scrollbars that will
    automatically show/hide as needed.'''
    @_create_container
    def __init__(self, master, **kw):
        Text.__init__(self, master, **kw)
        AutoScroll.__init__(self, master)

if __name__ == '__main__':
    vp_start_gui()



