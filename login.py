import os
from Tkinter import *

import chat
import chatclient
import signup

if os.path.exists("uname.txt"):
    if os.path.exists("sessid.txt"):
        chat.uname = open("uname.txt","rb").read()
        chat.sessid = open("sessid.txt","rb").read()
        chatclient.vp_start_gui()
        sys.exit(1)


def init(top, gui, *args, **kwargs):
    global w, top_level, root
    w = gui
    top_level = top
    root = top



def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = Tk()
    top = Login (root)
    init(root, top)
    root.mainloop()

w = None
def create_Login(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = Toplevel (root)
    top = Login (w)
    init(w, top, *args, **kwargs)
    return (w, top)

def destroy_Login():
    global w
    w.destroy()
    w = None
def destroy_window():
    # Function which closes the window.
    global top_level
    top_level.destroy()
    top_level = None

class Login:
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85' 
        _ana2color = '#d9d9d9' # X11 color: 'gray85' 

        top.geometry("528x191+367+275")
        top.title("Login")



        self.email = Entry(top)
        self.email.place(relx=0.06, rely=0.21, relheight=0.16, relwidth=0.9)
        self.email.configure(background="white")
        self.email.configure(font="TkFixedFont")
        self.email.configure(width=476)
        self.email.bind('<Return>', lambda: signin())

        self.emailText = Label(top)
        self.emailText.place(relx=0.02, rely=0.1, height=18, width=66)
        self.emailText.configure(text='''EMAIL:''')
        self.emailText.configure(width=66)

        self.password = Entry(top)
        self.password.place(relx=0.06, rely=0.52, relheight=0.16, relwidth=0.9)
        self.password.configure(background="white")
        self.password.configure(font="TkFixedFont")
        self.password.configure(width=476)
        self.password.configure(show="*")
        self.password.bind('<Return>', lambda e: signin())

        self.passwordText = Label(top)
        self.passwordText.place(relx=0.02, rely=0.42, height=18, width=86)
        self.passwordText.configure(text='''PASSWORD:''')
        self.passwordText.configure(width=86)
        def signin():
            if chat.login(self.email.get(), self.password.get(), rememberVar.get()):
                destroy_window()
                chatclient.vp_start_gui()

        self.loginButton = Button(top)
        self.loginButton.place(relx=0.81, rely=0.79, height=26, width=64)
        self.loginButton.configure(activebackground="#d9d9d9")
        self.loginButton.configure(command=lambda: signin())
        self.loginButton.configure(text='''LOGIN''')

        global rememberVar
        rememberVar = IntVar()
        self.remember = Checkbutton(top)
        self.remember.place(relx=0.02, rely=0.79, relheight=0.1, relwidth=0.23)
        self.remember.configure(activebackground="#d9d9d9")
        self.remember.configure(justify=LEFT)
        self.remember.configure(text='''REMEMBER ME''')
        self.remember.configure(variable=rememberVar)
        self.remember.configure(width=122)
        rememberVar.set(1)
        def rsignup():
            destroy_window()
            signup.vp_start_gui()
        self.signup = Button(top)
        self.signup.place(relx=0.66, rely=0.79, height=26, width=72)
        self.signup.configure(activebackground="#d9d9d9")
        self.signup.configure(command=lambda: rsignup())
        self.signup.configure(text='''SIGNUP''')







if __name__ == '__main__':
    vp_start_gui()



sys.exit()