import tkMessageBox
from Tkinter import *

import chat
import chatclient


def init(top, gui, *args, **kwargs):
    global w, top_level, root
    w = gui
    top_level = top
    root = top

def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = Tk()
    top = Sign_Up (root)
    init(root, top)
    root.mainloop()

w = None
def create_Sign_Up(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = Toplevel (root)
    top = Sign_Up (w)
    init(w, top, *args, **kwargs)
    return (w, top)

def destroy_Sign_Up():
    global w
    w.destroy()
    w = None
def destroy_window():
    # Function which closes the window.
    global top_level
    top_level.destroy()
    top_level = None

class Sign_Up:
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85' 
        _ana2color = '#d9d9d9' # X11 color: 'gray85' 

        top.geometry("423x460+501+154")
        top.title("Sign Up")



        self.SignupTEXT = Label(top)
        self.SignupTEXT.place(relx=0.43, rely=0.11, height=18, width=86)
        self.SignupTEXT.configure(text='''SIGN UP''')
        self.SignupTEXT.configure(width=86)

        self.NameTEXT = Label(top)
        self.NameTEXT.place(relx=0.05, rely=0.2, height=18, width=66)
        self.NameTEXT.configure(text='''NAME:''')
        self.NameTEXT.configure(width=66)

        self.NameEntry = Entry(top)
        self.NameEntry.place(relx=0.09, rely=0.26, relheight=0.07, relwidth=0.84)

        self.NameEntry.configure(background="white")
        self.NameEntry.configure(font="TkFixedFont")
        self.NameEntry.configure(width=356)

        self.EmailTEXT = Label(top)
        self.EmailTEXT.place(relx=0.05, rely=0.35, height=18, width=66)
        self.EmailTEXT.configure(text='''EMAIL:''')
        self.EmailTEXT.configure(width=66)

        self.EmailENTRY = Entry(top)
        self.EmailENTRY.place(relx=0.09, rely=0.39, relheight=0.07
                , relwidth=0.84)
        self.EmailENTRY.configure(background="white")
        self.EmailENTRY.configure(font="TkFixedFont")
        self.EmailENTRY.configure(width=356)

        self.UsernameTEXT = Label(top)
        self.UsernameTEXT.place(relx=0.05, rely=0.48, height=18, width=96)
        self.UsernameTEXT.configure(text='''USERNAME:''')
        self.UsernameTEXT.configure(width=96)

        self.UsernameENTRY = Entry(top)
        self.UsernameENTRY.place(relx=0.09, rely=0.52, relheight=0.07
                , relwidth=0.84)
        self.UsernameENTRY.configure(background="white")
        self.UsernameENTRY.configure(font="TkFixedFont")
        self.UsernameENTRY.configure(width=356)

        self.PasswordTEXT = Label(top)
        self.PasswordTEXT.place(relx=0.05, rely=0.61, height=18, width=96)
        self.PasswordTEXT.configure(text='''PASSWORD:''')
        self.PasswordTEXT.configure(width=96)

        self.PasswordENTRY = Entry(top)
        self.PasswordENTRY.place(relx=0.09, rely=0.65, relheight=0.07
                , relwidth=0.84)
        self.PasswordENTRY.configure(background="white")
        self.PasswordENTRY.configure(font="TkFixedFont")
        self.PasswordENTRY.configure(width=356)
        self.PasswordENTRY.configure(show="*")

        self.ConfirmTEXT = Label(top)
        self.ConfirmTEXT.place(relx=0.05, rely=0.74, height=18, width=156)
        self.ConfirmTEXT.configure(text='''CONFIRM PASSWORD:''')
        self.ConfirmTEXT.configure(width=156)

        self.ConfirmENTRY = Entry(top)
        self.ConfirmENTRY.place(relx=0.09, rely=0.78, relheight=0.07
                , relwidth=0.84)
        self.ConfirmENTRY.configure(background="white")
        self.ConfirmENTRY.configure(font="TkFixedFont")
        self.ConfirmENTRY.configure(width=356)
        self.ConfirmENTRY.configure(show="*")

        self.createAccount = Button(top)
        self.createAccount.place(relx=0.09, rely=0.89, height=26, width=357)
        self.createAccount.configure(activebackground="#d9d9d9")
        self.createAccount.configure(text='''CREATE ACCOUNT''')
        self.createAccount.configure(width=357)
        self.createAccount.configure(command=lambda: do_signup())


        def do_signup():
            if self.EmailENTRY.get().__contains__("@"):
               if self.PasswordENTRY.get() == self.ConfirmENTRY.get():
                   if self.NameEntry.get() != "" and self.UsernameENTRY.get() != "" and self.EmailENTRY.get() != "" and self.PasswordENTRY.get() !="" and self.ConfirmENTRY.get() != "":
                       if chat.signup(name=self.NameEntry.get(),email=self.EmailENTRY.get(),chatname=self.UsernameENTRY.get(),password=self.PasswordENTRY.get()):
                            destroy_window()
                            chatclient.vp_start_gui()
                   else:
                       tkMessageBox.showerror(message="One or more entrys is not filled out.")
               else:
                   tkMessageBox.showerror(message="Passwords do not match!")
            else:
                tkMessageBox.showerror(message="Email is invalid.")


if __name__ == '__main__':
    vp_start_gui()



